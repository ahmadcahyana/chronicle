# Getting Started
First clone the repository from Github and switch to the new directory:

```sh
$ git clone git@gitlab.com:ahmadcahyana/chronicle.git
$ cd chronicle
```

Activate the virtualenv for your project.
Install project dependencies:

```sh
$ pip install -r requirements.txt
```

Then simply apply the migrations:
```sh
$ python manage.py migrate
````

***if you want***, I have initial data:
```sh
$ python manage.py loaddata fixtures/persons.json --app persons.person
````

You can now run the development server:
```sh
$ python manage.py runserver
```

You can check endpoints via postman. just import to posman `postman/Chronicle.postman_collection.json` and `postman/Chronicle.postman_environment.json`