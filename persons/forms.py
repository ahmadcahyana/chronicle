from django import forms

from persons.models import Person


class PersonForm(forms.ModelForm):
    fName = forms.CharField(max_length=255, required=True)
    lName = forms.CharField(max_length=255, required=True)
    isActive = forms.BooleanField(required=False)

    class Meta:
        model = Person
        fields = ["id", "fName", "lName", "isActive"]

