import json

from django.forms.models import model_to_dict
from django.http import JsonResponse
from django.core.paginator import Paginator

from .forms import PersonForm
from .models import Person


def get_person_list(request):
    if request.method == 'GET':
        page = request.GET.get("page", 1)
        limit = request.GET.get("limit", 10)
        persons = Person.objects.all()
        paginator = Paginator(list(persons.values()), limit or 10)
        paginated_data = paginator.get_page(page or 1)
        data = {
            "page": paginated_data.number,
            "data": list(paginated_data),
            "total": paginator.count
        }
        return JsonResponse(data)
    if request.method == 'POST':
        content = json.loads(request.body)
        f = PersonForm(content)
        print(content)
        if f.is_valid():
            f.save()
            return JsonResponse({"message": "Successfully Created!"})
        else:
            print(f.errors)
            return JsonResponse({"message": f.errors}, status=400)


def get_person_detail(request, pk):
    try:
        person = Person.objects.get(pk=pk)
        if request.method == 'GET':
            return JsonResponse(model_to_dict(person))
        if request.method == 'DELETE':
            person.delete()
            return JsonResponse(data={"message": f"Person with id {pk} deleted!"})
        elif request.method == 'PUT':
            content = json.loads(request.body)
            f = PersonForm(content or None, instance=person)
            if not f.is_valid():
                print(f.data)
                return JsonResponse({"message": f.errors}, status=400)
            f.save()
            return JsonResponse(data={"message": f"Person with id {pk} updated!"}, status=201)
    except Person.DoesNotExist:
        return JsonResponse(data={"message": f"Person with id {pk} doesn't exist!"}, status=404)

