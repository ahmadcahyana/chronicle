from django.db import models


class Person(models.Model):
    fName = models.CharField(max_length=255)
    lName = models.CharField(max_length=255)
    isActive = models.BooleanField(default=True)
